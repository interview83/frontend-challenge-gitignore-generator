# Frontend challenge - Gitignore generator

## Objective
Design and implement a web application that allows users to generate `.gitignore` files based on their preferred programming languages using the gitignore.io API. The application should provide a user-friendly interface with buttons to edit selections and initiate API calls to retrieve and download the appropriate `.gitignore` file.

## Requirements

- **Frontend Interface:**
  - Create a frontend interface with buttons or checkboxes representing various programming languages and frameworks.
  - Provide an interface for users to select/deselect programming languages they want to include in the `.gitignore` file.
  - Design the frontend interface to be visually appealing and user-friendly.

- **API Integration:**
  - Utilize the gitignore.io API ([doc here](https://docs.gitignore.io/use/api)) to fetch `.gitignore` templates based on user selections.
  - Implement the necessary logic to construct the API request URL dynamically based on the user's language selections.

- **Download Functionality:**
  - Implement a mechanism to allow users to download the generated `.gitignore` file.
  - Ensure that the downloaded file contains the correct content based on the user's selections.

- **Error Handling (Optional):**
  - Implement error handling mechanisms to gracefully handle API errors or failures.
  - Notify users in case of any issues encountered during the generation or download process.

### Additional Tasks (Optional):

- Implement functionality to remember user selections between sessions using local storage or cookies.
- Write unit tests to ensure the reliability and correctness of the application's core functionalities.
- Ensure responsiveness across different devices and screen sizes for a seamless user experience.

## Submission Instructions

- **Forking the Repository:**
  - Please fork the provided repository to your personal account. All your work should be committed to this forked repository.

- **Running the Application:**
  - Please provide clear and detailed instructions on how to run your application. This should include steps to install any necessary dependencies, as well as how to start the application locally. These instructions should be added to the README file in your repository.

- **Code Quality:**
  - While we do not expect a fully working application, we will be evaluating the clarity and structure of your code. Please ensure your code is well-organized and includes comments where necessary to explain your logic.

- **Commit History:**
  - We will also be looking at your commit history. Please make regular commits with clear, descriptive messages. This helps us understand your development process.

Once you have completed your solution, please submit the URL of your forked repository. We look forward to seeing your solution!
